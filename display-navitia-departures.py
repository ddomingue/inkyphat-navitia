 #!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json
from datetime import datetime
from PIL import Image, ImageFont
import inkyphat

print("Display next train departures from selected station")

inkyphat.set_border(inkyphat.BLACK)
#inkyphat.set_rotation(180)

inkyphat.rectangle((0, 0, inkyphat.WIDTH, inkyphat.HEIGHT), fill=inkyphat.WHITE)

#font = inkyphat.ImageFont.truetype(inkyphat.fonts.AmaticSC, 22)
font = inkyphat.ImageFont.truetype("./fonts/battlenet.ttf",15)

offset_x, offset_y = 5, 5

#Call Navitia Rest Api with the departures from Sannois station 
def queryNavitiaDepartures():
    headers = {"Authorization":"NAVITIA-AUTH-TOKEN-HERE"}
    url = "https://api.navitia.io/v1/coverage/fr-idf/stop_areas/stop_area%3A0%3ASA%3A8727618/lines/line%3A0%3A800%3AJ/departures?"

    navitiaResponse = requests.get(url,headers=headers)

    return json.loads(navitiaResponse.text)

#Format a departure json block to departure time and last station
def formatDeparture(departure):
    display_information = departure['display_informations']
    train_code = display_information['code']
    train_headsign = display_information['headsign']
    direction = display_information['direction'].split('(')[0]

    departure_time = datetime.strptime(departure['stop_date_time']['departure_date_time'], '%Y%m%dT%H%M%S')
    departure_time_str = datetime.strftime(departure_time,"%H:%M")

    return "{0} {1}".format(departure_time_str, direction)

#Utility function
def printStrBlack(text):
    printStr(text, inkyphat.BLACK)

#Print a string taking into account the global offset_y parameter, takes multiline into account
def printStr(text, color):
    global offset_y
    if offset_y >= inkyphat.HEIGHT:
            return
    inkyphat.text((offset_x, offset_y), text, color, font=font)
    offset_y += font.getsize(text)[1] + 2

#Print a subset of departure objects, using the first nb_elements
def printDepartures(departures,nb_elements):
    formatted_departures = map(formatDeparture,departures[:nb_elements])
    map(printStrBlack,formatted_departures)

#Prints the last update call at the top of the inkyphat
def printLastUpdate():
    info_str = "LINE J (Sannois) at {0}".format(datetime.strftime(datetime.now(),"%H:%M"))
    printStr(info_str, inkyphat.RED)

#Organize display using the functions defined previously
def displayNextDeparturesFromSannois(nb_elements):
    printLastUpdate()
    nav = queryNavitiaDepartures()
    departures =  nav['departures']
    printDepartures(departures,nb_elements)

#Current called code on script call :

displayNextDeparturesFromSannois(6)
inkyphat.set_colour('red')
inkyphat.show()
