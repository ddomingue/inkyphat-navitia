# navitia-inkyphat

Python script to display next departures and arrivals from a navitia.io query onto an [inkyphat](https://shop.pimoroni.com/products/inky-phat?variant=12549254217811) EPD installed on a raspberry pi. 
To use, type "python display-navitia-departures.py" in a terminal

![screenshot_test](readme/navitia_inky_test.png)

# Requirements

Requires the inkyphat and PIL libraries installed, if you have an inkyphat installed it should already be the case

# InkyPhat documentation

Extensive documentation for the inkyphat is available here : 

https://github.com/pimoroni/inky-phat

# Navitia.io specifics

Before using this script, you should create a [navitia](https://www.navitia.io/) account and add an authentication token, which is required in the script
(see queryNavitiaDepartures() function)

## Finding your station

You can find your train station code using the [navitia playground](http://canaltp.github.io/navitia-playground/play.html) autocomplete on the stop_areas element, see below. The playground also requires a valid token for actual coverages.

![screenshot_stop_areas](readme/navitia_stop_area.png)

You can then use the URL generated on the page in the queryNavitiaDepartures() method of the script.
